---
title: "4. GitLab GitOps via The GitLab Kubernetes Agent"
weight: 40
chapter: true
draft: false
desscription: "Using the Gitlab K8s Agent to deploy applications to EKS clusters through a Pull Agent"
## In this s#pre: '<i class="fa fa-film" aria-hidden="true"></i> '
---

## In this section
{{% children style="h3" description="true" %}}